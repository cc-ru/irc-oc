local function quad(x)
  return x^2
end

local ThrottlingScheduler do
  local meta = {
    __index = {
      push = function(self, item, remitDelay)
        self:__updateProcessed()
        local now = self.timeFunction()
        local delay = math.max(0, self.lastScheduledRun - now)

        if not remitDelay then
          local rate = math.min(1, #self.processed / self.maxThroughput)
          delay = delay + self.delayFunction(rate) * self.maxDelay
        end

        local scheduleTime = now + delay

        table.insert(self.processed, 1, scheduleTime)
        self.lastScheduledRun = self.scheduler(item, delay, scheduleTime)
        assert(type(self.lastScheduledRun) == "number",
               "scheduler must return number")

        return self.lastScheduledRun
      end,

      __updateProcessed = function(self)
        local now = self.timeFunction()

        for i = #self.processed, 1, -1 do
          if self.processed[i] + self.maxDelay < now then
            self.processed[i] = nil
          end
        end
      end,
    }
  }

  -- Requirements:
  -- 1. The `scheduler` returns the time when it will process an item
  -- 2. The time function increases monotonically.
  -- 3. The `scheduler` use a compatible time function.
  -- 4. If multiple items are to be processed at the same time,
  --    it must be done in the FIFO order of the `scheduler` calls.
  -- 5. The delay function t(rate) must satisfy all the following properties:
  --    - for ∀ rate ∈ [0; 1], t(rate) ∈ [0; 1];
  --    - t(0) = 0
  --    - t(1) = 1
  --    - t increases monotonically.
  --    E.g., t(rate) = rate^k (k > 0).
  function ThrottlingScheduler(maxDelay, maxThroughput, scheduler)
    local self = {
      processed = {},
      maxDelay = maxDelay,
      maxThroughput = maxThroughput,
      timeFunction = require("computer").uptime,
      delayFunction = quad,
      scheduler = scheduler,
      lastScheduledRun = -math.huge,
    }

    return setmetatable(self, meta)
  end
end

return ThrottlingScheduler
