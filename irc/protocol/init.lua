local escapeTranslation = {
  [";"] = "\\;",
  [" "] = "\\s",
  ["\\"] = "\\\\",
  ["\r"] = "\\r",
  ["\n"] = "\\n",
}

local unescapeTranslation = {
  [";"] = ";",
  ["s"] = " ",
  ["\\"] = "\\",
  ["r"] = "\r",
  ["n"] = "\n",
}

local function escapeTag(value)
  return value:gsub("[; \\\r\n]", escapeTranslation)
end

local function unescapeTag(escaped)
  return escaped:gsub("\\([^;s\\rn]?)", "%1")
                :gsub("\\(.)", unescapeTranslation)
end

local function compileLine(tags, command, parameters)
  local line = ""

  if #tags > 0 then
    local tagPart = {}

    for k, v in pairs(tags) do
      if type(k) == "number" then
        table.insert(tagPart, v)
      elseif type(v) ~= "string" then
        table.insert(tagPart, k)
      else
        table.insert(tagPart, ("%s=%s"):format(k, escapeTag(v)))
      end
    end

    line = "@" .. table.concat(tagPart, ";") .. " "
  end

  line = line .. command

  local compiledParams = {}

  for i, v in ipairs(parameters) do
    v = tostring(v)

    assert(not v:find(" ") or i == #parameters,
           ("bad parameter #%d: spaces not allowed here"):format(i))

    if i == #parameters and v:find(" ") then
      v = ":" .. v
    end

    compiledParams[i] = v
  end

  if #compiledParams > 0 then
    line = line .. " " .. table.concat(compiledParams, " ")
  end

  return line .. "\r\n"
end

local Source do
  local meta = {
    __tostring = function(self)
      return self.source
    end,

    __metatable = "source",
  }

  function Source(source)
    local self = {
      source = source,
    }

    self.nickname,
    self.username,
    self.host = source:match("^([^!]+)!([^@]+)@(.+)$")

    if not self.nickname then
      self.nickname, self.host = source:match("^([^@]+)@(.+)$")
    end

    if not self.nickname then
      self.nickname = source
    end

    return setmetatable(self, meta)
  end
end

local function parseLine(line)
  local tags, parameters = {}, {}
  local source, command
  local i = 1

  if line:sub(1, 1) == "@" then
    -- parse tags
    local tagPart
    tagPart, i = line:match("(%S+)%s+()", 2)

    for tag in tagPart:gmatch("[^;]+") do
      local key, value = tag:match("^([^=]+)=(.*)$")

      if key then
        tags[key] = unescapeTag(value)
      else
        tags[tag] = true
      end
    end
  end

  if line:sub(i, i) == ":" then
    source, i = line:match("(%S+)%s+()", i + 1)
    source = Source(source)
  end

  command, i = line:match("(%S+)%s*()", i)
  command = command:upper()

  while true do
    if line:sub(i, i) == ":" then
      table.insert(parameters, line:sub(i + 1))
      break
    end

    local param
    param, i = line:match("(%S+)%s*()", i)

    if not param then
      break
    end

    table.insert(parameters, param)
  end

  return tags, source, command, parameters
end

return {
  compileLine = compileLine,
  parseLine = parseLine,
  Source = Source,
}
