local function getEndOfLastUtf8Char(message, hardLimit)
  if message:sub(hardLimit):byte() < 0x80 then
    -- bit 8 not set
    return hardLimit
  else
    for i = hardLimit, hardLimit - 3, -1 do
      local char = message:sub(i, i)
      local byte = char:byte()

      if byte < 0x80 then
        -- invalid UTF-8
        return i
      elseif byte >= 0x80 and byte < 0xc0 then
        -- 0b10xxxxxx: continuation
        -- skip
      elseif byte >= 0xc0 then
        -- 0b11xxxxxx: start of UTF-8 sequence
        return i - 1
      end
    end

    -- invalid UTF-8
    return hardLimit
  end
end

local function splitter(message, size)
  local lines = {}

  while true do
    if #lines > 0 then
      -- strip trailing spaces
      message = message:sub(message:match("^%s*()"))
    end

    if #message == 0 then
      break
    end

    local chunk = message:sub(1, size + 1)
    local line

    if chunk:find("\n") then
      line, message = message:match("^([^\n]*)\n(.*)$")
    elseif #chunk < size + 1 then
      line, message = chunk, ""
    else
      local lastSpacePos, spaceEnd = chunk:match("^.+()%s+()")

      if lastSpacePos and lastSpacePos >= size / 2 then
        line = message:sub(1, lastSpacePos - 1)
        message = message:sub(spaceEnd)
      else
        local pos = getEndOfLastUtf8Char(message, size)
        line = message:sub(1, pos)
        message = message:sub(pos + 1)
      end
    end

    table.insert(lines, line)
  end

  return lines
end

return splitter
