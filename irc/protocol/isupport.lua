-- Lua 5.2 fallback
local utf8 = utf8 or {
  charpattern = "[\0-\x7F\xC2-\xF4][\x80-\xBF]*",
}

local function limitValue(value)
  if value == "" then
    value = math.huge
  end

  return tonumber(value)
end

local isupportHandlers = {
  ACCEPT = function(self, value)
    self.features.maxAcceptCount = tonumber(value)
  end,

  AWAYLEN = function(self, value)
    self.features.maxAwayLength = limitValue(value)
  end,

  CALLERID = function(self, value)
    value = value or "g"
    self.features.callerId = value
  end,

  CASEMAPPING = function(self, value)
    self.features.caseMapping = value:lower()
  end,

  CHANLIMIT = function(self, value)
    self.features.channelLimit = {}

    for prefixes, limit in value:gmatch("([^:]+):(%d*)") do
      limit = limitValue(limit)

      for prefix in prefixes:gmatch(utf8.charpattern) do
        self.features.channelLimit[prefix] = limit
      end
    end
  end,

  CHANMODES = function(self, value)
    local channelModes = {
      -- the server always sends a parameter
      server = {},
      -- both the server and the client send a parameter
      always = {},
      -- a parameter is sent only when the mode is set
      onlySet = {},
      -- the mode has no parameters
      never = {},
    }

    channelModes.modes = {}

    local groupTypes = {
      "server", "always", "onlySet", "never"
    }

    local i, pos = 1, 1

    repeat
      local groupType = groupTypes[i]
      local group
      group, pos = value:match("([^,]*),?()", pos)

      for char in group:gmatch(utf8.charpattern) do
        channelModes[groupType][char] = true
        channelModes.modes[char] = groupType
      end

      i = i + 1
    until pos > #value


    self.features.channelModes = channelModes
  end,

  CHANNELLEN = function(self, value)
    self.features.maxChannelLength = tonumber(value)
  end,

  CHANTYPES = function(self, value)
    self.features.channelTypes = {}

    for char in value:gmatch(utf8.charpattern) do
      self.features.channelTypes[char] = true
    end
  end,

  CNOTICE = function(self)
    self.features.cnotice = true
  end,

  CPRIVMSG = function(self)
    self.features.cprivmsg = true
  end,

  DEAF = function(self, value)
    self.features.deaf = value
  end,

  ELIST = function(self, value)
    local elist = {}

    elist.channelCreationTime = not not value:find("C")
    elist.mask = not not value:find("M")
    elist.inverseMask = not not value:find("N")
    elist.topicSetTime = not not value:find("T")
    elist.userCount = not not value:find("U")

    self.features.elist = elist
  end,

  EXCEPTS = function(self, value)
    self.features.banExemptions = value or "e"
  end,

  EXTBAN = function(self, value)
    self.features.extendedBans = {}

    local prefix, types = value:match("^(.?),(.+)$")
    self.features.extendedBans.prefix = prefix

    for char in types:gmatch(utf8.charpattern) do
      self.features.extendedBans[char] = true
    end
  end,

  INVEX = function(self, value)
    self.features.inviteExceptions = value or "I"
  end,

  KEYLEN = function(self, value)
    self.features.maxKeyLength = limitValue(value)
  end,

  KICKLEN = function(self, value)
    self.features.maxKickLength = limitValue(value)
  end,

  KNOCK = function(self)
    self.features.knock = true
  end,

  MAXLIST = function(self, value)
    self.features.maxListLength = {}

    for modes, limit in value:gmatch("([^:]+):(%d+)") do
      limit = tonumber(limit)

      for char in modes:gmatch(utf8.charpattern) do
        self.features.maxListLength[char] = limit
      end
    end
  end,

  METADATA = function(self, value)
    self.features.maxMetadataCount = limitValue(value)
  end,

  MODES = function(self, value)
    self.features.maxVarModesInCommand = limitValue(value)
  end,

  MONITOR = function(self, value)
    self.features.maxMonitorCount = limitValue(value)
  end,

  NETWORK = function(self, value)
    self.features.networkName = value
  end,

  NICKLEN = function(self, value)
    self.features.maxNickLength = limitValue(value)
  end,

  PREFIX = function(self, value)
    self.features.membershipPrefixes = {
      modes = {},
      prefixes = {},
    }

    if value == "" then
      return
    end

    local tbl = self.features.membershipPrefixes

    local modes, prefixes = value:match("^%(([^)]+)%)(.+)$")
    local modeIter = modes:gmatch(utf8.charpattern)
    local prefixIter = prefixes:gmatch(utf8.charpattern)

    for mode, prefix
        in function()
          -- ad-hoc zip
          return modeIter(), prefixIter()
        end do
      tbl.modes[mode] = prefix
      table.insert(tbl.modes, mode)

      tbl.prefixes[prefix] = mode
      table.insert(tbl.prefixes, prefix)
    end
  end,

  SAFELIST = function(self)
    self.features.safeList = true
  end,

  SILENCE = function(self, value)
    self.features.maxSilenceCount = limitValue(value)
  end,

  STATUSMSG = function(self, value)
    self.features.statusMessage = {}

    for char in value:gmatch(utf8.charpattern) do
      self.features.statusMessage[char] = true
    end
  end,

  TARGMAX = function(self, value)
    self.features.maxTargetCount = {}

    for command, limit in value:gmatch("([^:]+):(%d*)") do
      self.features.maxTargetCount[command] = limitValue(limit)
    end
  end,

  TOPICLEN = function(self, value)
    self.features.maxTopicLength = tonumber(value)
  end,

  USERIP = function(self)
    self.features.userIp = true
  end,

  USERLEN = function(self, value)
    self.features.maxUsernameLength = limitValue(value)
  end,

  WHOX = function(self)
    self.features.extendedWho = true
  end,
}

return isupportHandlers
