local function checkSubscribeArgs(events)
  if type(events[events.n]) == "number" then
    -- :subscribe(Event, priority, callback)
    events = table.pack(events)
  end

  for i = 1, events.n, 1 do
    local arg = events[i]

    assert(type(arg[1]) == "table",
           ("bad first value in target #%d: Event expected, got %s")
             :format(i, type(arg[1])))
    assert(type(arg[2]) == "number",
           ("bad priority value in target #%d: number expected, got %s")
             :format(i, type(arg[2])))
  end

  return events
end

local PriorityQueue do
  local meta = {
    __index = {
      push = function(self, item, priority)
        for i, v in ipairs(self) do
          if v[2] <= priority then
            table.insert(self, i, {item, priority})
            return
          end
        end

        table.insert(self, {item, priority})
      end,

      remove = function(self, item)
        for i, v in ipairs(self) do
          if v[1] == item then
            table.remove(self, i)
            return true
          end
        end
      end,
    },

    __pairs = function(self)
      local snapshot = {}

      for i = 1, #self, 1 do
        snapshot[i] = self[i]
      end

      local i = 0

      return function()
        if i == #snapshot then
          return
        end

        i = i + 1

        return snapshot[i][1], snapshot[i][2]
      end
    end,
  }

  function PriorityQueue()
    return setmetatable({}, meta)
  end
end

local Event do
  local methods = {
    cancel = function(self)
      self.canceled = true
    end,

    get = function(self)
      return self.__data
    end,

    new = function(self, data)
      local obj = {
        canceled = false,
        __data = data or {},
      }

      return setmetatable(obj, {
        __index = function(obj, key)
          if self[key] ~= nil then
            return self[key]
          elseif obj.__data[key] ~= nil then
            return obj.__data[key]
          end
        end,

        __tostring = function(obj)
          return ("Event { name = %q }"):format(tostring(obj._name))
        end,
      })
    end,
  }

  function Event(name)
    local self = {
      _name = name,
    }

    self.__class = self

    return setmetatable(self, {
      __call = function(self, ...)
        return self:new(...)
      end,

      __index = methods,

      __tostring = function(obj)
        return ("Event { name = %q }"):format(tostring(obj._name))
      end,
    })
  end
end

local Handler do
  local meta = {
    __call = function(self, ...)
      local nextEvents, message = self:run(...)

      if not nextEvents then
        self:remove()

        return nil, message
      end

      if #nextEvents == 0 then
        return self.defaultEvents
      end

      return nextEvents
    end,

    __index = {
      remove = function(self)
        self.bus:remove(self)
        self.__removed = true
      end,

      await = function(self, ...)
        if type(self.callback) ~= "thread" then
          error("callback must be a coroutine")
        end

        checkSubscribeArgs(table.pack(...))

        return coroutine.yield(...)
      end,

      run = function(self, ...)
        local message

        if type(self.callback) == "thread" then
          local nextEvents

          if not self.__consecutiveRun then
            nextEvents = {coroutine.resume(self.callback,
                                           self, self.bus.state, ...)}
            self.__consecutiveRun = true
          else
            nextEvents = {coroutine.resume(self.callback, ...)}
          end

          local success = table.remove(nextEvents, 1)

          if not success then
            message = self.bus.errorHandler(self, nextEvents[1], ...)
          else
            if coroutine.status(self.callback) == "dead" then
              -- coroutine returned
              self:remove()
            end

            return nextEvents
          end
        else
          local args = table.pack(...)

          local nextEvents = {xpcall(
            function(...)
              return table.unpack(
                checkSubscribeArgs(table.pack(self.callback(...)))
              )
            end,
            function(msg)
              return self.bus.errorHandler(self, msg,
                                           table.unpack(args, 1, args.n))
            end,
            self, self.bus.state, ...
          )}

          local success = table.remove(nextEvents, 1)

          if not success then
            message = nextEvents[1]
          else
            return nextEvents
          end
        end

        if not message then
          return self.events
        end

        -- remove
        return nil, message
      end,
    },
  }

  function Handler(bus, events, callback)
    local self = {
      bus = bus,
      defaultEvents = events,
      events = events,
      callback = callback,
      __removed = false,
    }

    return setmetatable(self, meta)
  end
end

local EventBus do
  local meta = {
    __tostring = function(self)
      return ("EventBus { name = %q, #queue = %d }"):format(
        tostring(self.name),
        #self.__queue
      )
    end,

    __index = {
      push = function(self, evt, force)
        assert(type(evt) == "table",
               ("bad argument #1: Event expected, got %s"):format(type(evt)))

        if force then
          self:__dispatch(evt, force)
        else
          table.insert(self.__queue, 1, evt)

          if self.__running then
            return
          end

          self.__running = true

          while #self.__queue > 0 do
            self:__dispatch(table.remove(self.__queue))
          end

          self.__running = false
        end
      end,

      remove = function(self, handler, evt)
        assert(type(handler) == "table",
               ("bad argument #1: Handler expected, got %s")
                 :format(type(handler)))
        assert(not evt or type(evt) == "table",
               ("bad argument #2: Event or nil expected, got %s")
                 :format(type(evt)))

        if evt then
          return self.__map[evt]:remove(handler)
        end

        for _, e in ipairs(handler.events) do
          self:remove(handler, e[1])
        end
      end,

      subscribe = function(self, ...)
        local events = table.pack(...)
        local callback = table.remove(events, events.n)
        events.n = events.n - 1

        events = checkSubscribeArgs(events)

        local handler = Handler(self, events, callback)

        for _, e in ipairs(events) do
          local evt, priority = e[1], e[2]
          self:__attach(handler, evt, priority)
        end

        return handler
      end,

      __dispatch = function(self, evt, forced)
        local handlers = self.__map[evt.__class] or {}

        for handler, priority in pairs(handlers) do
          local nextEvents, message = handler(evt)

          if not nextEvents then
            if not forced then
              self.__running = false
            end

            error(("Error in handler of %s on %s: %s"):format(
              evt, self, message
            ))
          end

          if not handler.__removed and handler.events ~= nextEvents then
            self:__reattach(handler, nextEvents)
          end

          if evt.canceled then
            break
          end
        end
      end,

      __attach = function(self, handler, evt, priority)
        self.__map[evt] = self.__map[evt] or PriorityQueue()
        self.__map[evt]:push(handler, priority)
      end,

      __reattach = function(self, handler, nextEvents)
        self:remove(handler)

        for _, e in ipairs(nextEvents) do
          local evt, priority = e[1], e[2]
          self:__attach(handler, evt, priority)
        end

        handler.events = nextEvents
      end,
    },
  }

  function EventBus(state, name)
    local self = {
      name = name,
      state = state,
      errorHandler = function(self, msg)
        if type(self.callback) == "thread" then
          return debug.traceback(self.callback, msg)
        end

        return debug.traceback(msg)
      end,
      __queue = {},
      __map = {},
      __running = false,
    }

    return setmetatable(self, meta)
  end
end

return {
  Event = Event,
  EventBus = EventBus,
}
