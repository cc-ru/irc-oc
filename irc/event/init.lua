local eventBus = require("irc.event.bus")

local enum = require("irc.enum")
local commands = enum.commands
local getCommandName = enum.getCommandName

local protocol = require("irc.protocol")
local compileLine = protocol.compileLine

local events = {
  irc = {
    commands = {},
  },
  client = {},
  capability = {},
}

local IrcEvent do
  local new = eventBus.Event("temporary event").new

  local function newOverride(self, client, data)
    local obj = new(self, data)
    obj.client = client

    return obj
  end

  function IrcEvent(name)
    local event = eventBus.Event(name)
    event.new = newOverride

    return event
  end
end

events.client.disconnected = IrcEvent("client.disconnected")
events.client.connected = IrcEvent("client.connected")
events.client.registered = IrcEvent("client.registered")
events.client.authenticated = IrcEvent("client.authenticated")
events.client.unknownCommand = IrcEvent("client.unknownCommand")
events.client.stopping = IrcEvent("client.stopping")
events.client.error = IrcEvent("client.error")

events.irc.send = IrcEvent("irc.send")

function events.irc.send:getLine()
  return compileLine(self.tags or {}, self.command, self.parameters)
end

events.irc.message = IrcEvent("irc.message")

function events.irc.message:reply(msg, ...)
  local target = self.source.nickname

  if self.client:isChannel(self.target) then
    msg = ("%s: %s"):format(self.source.nickname, msg)
    target = self.target
  end

  return self.client:msg(target, msg, ...)
end

events.irc.notice = IrcEvent("irc.notice")
events.irc.write = IrcEvent("irc.write")

events.irc.ctcpRequest = IrcEvent("irc.ctcpRequest")

function events.irc.ctcpRequest:reply(msg, ...)
  msg = ("\1%s %s\1"):format(self.ctcpCommand, msg)
  return self.client:notice(self.source.nickname, msg, ...)
end

events.irc.ctcpResponse = IrcEvent("irc.ctcpResponse")

-- TODO: client:ctcp
function events.irc.ctcpResponse:reply(msg, ...)
  msg = ("\1%s %s\1"):format(self.ctcpCommand, msg)
  return self.client:msg(self.source.nickname, msg, ...)
end

for k, v in pairs(commands) do
  local name = getCommandName(k)

  events.irc.commands[name] = IrcEvent("irc.command." .. name)
end

events.irc.command = IrcEvent("irc.command")

events.capability.new = IrcEvent("capability.new")
events.capability.removed = IrcEvent("capability.removed")
events.capability.acknowledged = IrcEvent("capability.acknowledged")
events.capability.notAcknowledged = IrcEvent("capability.notAcknowledged")
events.capability.negotiated = IrcEvent("capability.negotiated")

local priority = {
  top = 100,
  high = 75,
  normal = 50,
  low = 25,
  bottom = 0,
}

events.priority = priority

return events
