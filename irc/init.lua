local event = require("event")

local lib = {}

local eventBus = require("irc.event.bus")
local events = require("irc.event")

lib.commands = require("irc.enum").commands
lib.events = require("irc.event")

local compileLine = require("irc.protocol").compileLine
local splitter = require("irc.protocol.splitter")

local clientMeta = require("irc.client")
local ThrottlingScheduler = require("irc.throttlingScheduler")

local sendMeta do
  local LineWriter do
    local meta = {
      __call = function(self)
        if self.client.socket and self.socketId == self.client.socket.id() then
          local evt = lib.events.irc.write(self.client, self)
          self.client.bus:push(evt, true)

          if not evt.canceled and
              -- double-check: could've changed while pushing
              self.client.socket and
              self.socketId == self.client.socket.id() then
            local status, result, reason = xpcall(self.client.buffer.write,
                                                  debug.traceback,
                                                  self.client.buffer,
                                                  self.line)

            if status and not result then
              status, result = result, reason
            end

            if not status then
              if not self.client:isConnected() then
                self.client.bus:push(
                  lib.events.client.disconnected(self.client)
                )
              else
                error(reason)
              end
            end
          end
        end
      end,

      __tostring = function(self)
        return ("LineWriter { client = %s, line = %q, socketId = %q }"):format(
          self.client,
          self.line,
          self.socketId
        )
      end,
    }

    function LineWriter(line, client)
      local self = {
        line = line,
        client = client,
        socketId = client.socket.id(),
      }

      return setmetatable(self, meta)
    end
  end

  local function verb(commandName, throttle, split)
    return function(self, ...)
      local options = ...

      if type((...)) ~= "table" then
        options = {...}
      end

      options.command = commandName
      options.throttle = throttle
      options.split = split

      return self(options)
    end
  end

  sendMeta = {
    __call = function(self, ...)
      if self.client.__socketLock then
        return false, "socket is locked"
      end

      checkArg(1, (...), "string", "table")

      local tags, command, parameters, throttle = {}
      local split = false

      if type((...)) == "table" then
        local options = ...

        tags = options.tags or tags

        if type(options.command) == "string" then
          command = options.command
        else
          command = table.remove(options, 1)
          checkArg(1, command, "string")
        end

        throttle = options.throttle
        split = options.split
        parameters = options
      else
        command = ...
        parameters = {select(2, ...)}
      end

      local evt = events.irc.send(self.client, setmetatable({
        tags = tags,
        command = command,
        parameters = parameters
      }, {__index = parameters, __newindex = parameters}))

      self.client.bus:push(evt, true)

      local connectionSettings = self.client.settings.connection

      if #evt:getLine() > connectionSettings.maxLineLength and split then
        local lastParam = evt.parameters[#evt.parameters]
        local prefixSize = #evt:getLine() - #lastParam
        local size = connectionSettings.maxLineLength - prefixSize

        for _, msg in ipairs(connectionSettings.splitter(lastParam, size)) do
          parameters[#parameters] = msg
          local line = compileLine(tags, command, parameters)
          self.client.__writeScheduler:push(
            LineWriter(line, self.client),
            not throttle
          )
        end
      else
        self.client.__writeScheduler:push(
          LineWriter(evt:getLine(), self.client),
          not throttle
        )
      end
    end,

    __index = {
      cap = verb("CAP"),
      authenticate = verb("AUTHENTICATE"),
      pass = verb("PASS"),
      nick = verb("NICK"),
      user = verb("USER"),
      oper = verb("OPER"),
      quit = verb("QUIT"),
      join = verb("JOIN", true),
      part = verb("PART"),
      topic = verb("TOPIC", true),
      names = verb("NAMES"),
      list = verb("LIST"),
      motd = verb("MOTD"),
      version = verb("VERSION"),
      admin = verb("ADMIN"),
      connect = verb("CONNECT"),
      time = verb("TIME"),
      stats = verb("STATS"),
      info = verb("INFO"),
      mode = verb("MODE", true),
      privmsg = verb("PRIVMSG", true, true),
      notice = verb("NOTICE", true, true),
      userhost = verb("USERHOST"),
      kill = verb("KILL"),
      invite = verb("INVITE"),
      kick = verb("KICK"),
      lusers = verb("LUSERS"),
      who = verb("WHO"),
      whois = verb("WHOIS"),
      whowas = verb("WHOWAS"),
      ping = verb("PING"),
      pong = verb("PONG"),
      away = verb("AWAY"),
      ison = verb("ISON"),
    }
  }
end

local SchedulerCallback do
  local meta = {
    __call = function(self, callable, delay, scheduleTime)
      if not self[scheduleTime] then
        self[scheduleTime] = {}

        event.timer(delay, function(...)
          if not self[scheduleTime] then
            return
          end

          for _, callback in ipairs(self[scheduleTime]) do
            callback(...)
          end

          self[scheduleTime] = nil
        end, 1)
      end

      table.insert(self[scheduleTime], callable)

      return scheduleTime
    end,

    __len = function(self)
      local count = 0

      for _, callbacks in pairs(self) do
        count = count + #callbacks
      end

      return count
    end,

    __index = {
      clear = function(self)
        for _, callbacks in pairs(self) do
          self[callbacks] = nil
        end
      end,
    }
  }

  function SchedulerCallback()
    return setmetatable({}, meta)
  end
end

local function setOption(group, key, name, value, expectedType, validator)
  if value == nil then
    return group[key]
  end

  if type(expectedType) == "string" then
    assert(type(value) == expectedType,
           ("bad option value %s: %s expected"):format(name, expectedType))
  else
    for _, t in ipairs(expectedType) do
      if type(value) == t then
        goto checksPassed
      end
    end

    local message = table.unpack(table.pack(expectedType))

    if #message == 2 then
      message = ("%s or %s"):format(message[1], message[2])
    else
      if #message > 2 then
        message[#message] = ("or %s"):format(message[#message])
      end

      message = table.concat(message, ", ")
    end

    error(("bad option value %s: %s expected"):format(name, message))
  end

  ::checksPassed::

  if validator then
    local status, reason = validator(value)

    assert(
      status,
      ("bad option value %s: %s"):format(name, reason or "validation failed")
    )

    value = reason
  end

  group[key] = value

  return value
end

local function nonNegativeValidator(value)
  if value >= 0 then
    return true, value
  end

  return nil, "must be non-negative"
end

local builderMeta = {
  __index = {
    isReady = function(self)
      local settings = self.settings

      if not settings.connection.__ready then
        return false, "connection settings not set"
      elseif not settings.auth.__ready then
        return false, "auth settings not set"
      elseif not settings.account.__ready then
        return false, "account settings not set"
      elseif not settings.bot.__ready then
        return false, "bot settings not set"
      elseif not settings.execution.__ready then
        return false, "execution settings not set"
      end

      return true
    end,

    build = function(self)
      assert(self:isReady())
      self.send = setmetatable({
        client = self,
      }, sendMeta)

      self.__writeScheduler = ThrottlingScheduler(
        self.settings.connection.throttling.maxDelay,
        self.settings.connection.throttling.maxThroughput,
        SchedulerCallback()
      )

      if self.settings.connection.throttling.delayFunction then
        self.__writeScheduler.delayFunction = self.settings
          .connection.throttling.delayFunction
      end

      if self.settings.execution.reconnect then
        self.__reconnectScheduler = ThrottlingScheduler(
          180, 6, SchedulerCallback()
        )

        self.__reconnectScheduler.delayFunction = function(rate)
          return rate^1.6
        end
      end

      if self.settings.execution.catchErrors then
        self.bus.errorHandler = function(handler, msg, evt)
          local traceback

          if type(handler.callback) == "thread" then
            traceback = debug.traceback(handler.callback, msg)
          else
            traceback = debug.traceback(msg)
          end

          if handler.userDefined and evt.__class ~= lib.events.client.error then
            self.bus:push(lib.events.client.error(self, {
              traceback = traceback,
              handler = handler,
              message = msg,
            }), true)
          else
            return traceback
          end
        end
      end

      return setmetatable(self, clientMeta)
    end,

    buildAndRun = function(self)
      self = self:build()
      self:run()

      return self
    end,

    connection = function(self, options)
      local group = self.settings.connection

      setOption(group, "host", "connection.host", options.host, "string")
      setOption(group, "tls", "connection.tls", options.tls, "boolean")
      setOption(group, "timeout", "connection.timeout", options.timeout,
                "number", nonNegativeValidator)
      setOption(group, "bufferSize", "connection.bufferSize",
                options.bufferSize, "number", nonNegativeValidator)
      setOption(group, "throttling", "connection.throttling",
                options.throttling, "table",
                function(value)
                  local maxDelay = value.maxDelay or value[1] or 0
                  local maxThroughput = value.maxThroughput or value[2] or
                                        math.huge
                  local delayFunction = value.delayFunction

                  if type(maxDelay) ~= "number" then
                    return false, "maxDelay must be a number"
                  end

                  if type(maxThroughput) ~= "number" then
                    return false, "maxThroughput must be a number"
                  end

                  if delayFunction and type(delayFunction) ~= "function" then
                    return false, "delayFunction must be a function"
                  end

                  return true, {
                    maxDelay = maxDelay,
                    maxThroughput = maxThroughput,
                    delayFunction = delayFunction
                  }
                end)
      setOption(group, "splitter", "connection.splitter", options.splitter,
                "function")
      setOption(group, "maxLineLength", "connection.maxLineLength",
                options.maxLineLength, "number", nonNegativeValidator)

      if group.bufferSize == 0 then
        group.bufferSize = "no"
      end

      group.__ready = group.host ~= nil

      return self
    end,

    auth = function(self, options)
      local group = self.settings.auth

      setOption(group, "nickname", "auth.nickname", options.nickname, "string")
      setOption(group, "username", "auth.username", options.username, "string")
      setOption(group, "realname", "auth.realname", options.realname, "string")

      group.__ready = group.nickname ~= nil

      return self
    end,

    account = function(self, options)
      local group = self.settings.account

      setOption(group, "nickname", "account.nickname", options.nickname,
                "string")

      setOption(group, "password", "account.password", options.password,
                "string")

      group.__ready = not options.nickname or options.password ~= nil

      return self
    end,

    bot = function(self, options)
      local group = self.settings.bot

      setOption(
        group, "channels", "bot.channels", options.channels, "table",
        function(value)
          local channels = {}

          for k, v in pairs(value) do
            local tk, tv = type(k), type(v)

            if tk == "number" and tv == "string" then
              channels[v] = true
            elseif tk == "number" and tv == "table" and
                type(v.channel or v[1]) == "string" and
                (type(v.key or v[2]) == "string" or not (v.key or v[2])) then
              channels[v.channel or v[1]] = v.key or v[2] or true
            elseif tk == "string" and tv ~= "string" then
              channels[k] = true
            elseif tk == "string" and tv == "string" then
              channels[k] = v
            else
              return nil, ("invalid value %s => %s"):format(k, v)
            end
          end

          return true, channels
        end
      )
      setOption(
        group, "tracking", "bot.tracking", options.tracking, "table",
        function(value)
          local result = group.tracking

          for k, v in pairs(value) do
            assert(result[k] ~= nil,
                   ("unknown key: %s in bot.tracking"):format(k))
            assert(
              not v or v == true or v == "lazy",
              ('bad value of bot.tracking.%s: false, true, or "lazy" expected')
                :format(k)
            )
            assert(v ~= "lazy", '"lazy" is not yet supported')

            result[k] = v
          end

          return true, result
        end
      )

      return self
    end,

    execution = function(self, options)
      local group = self.settings.execution

      setOption(group, "threaded", "execution.threaded", options.threaded,
                "boolean")
      setOption(group, "reconnect", "execution.reconnect", options.reconnect,
                "boolean")
      setOption(group, "catchErrors", "execution.catchErrors",
                options.catchErrors, "boolean")

      return self
    end,

    subscribe = clientMeta.__index.subscribe,
  },

  __tostring = function(self)
    local ready, reason = self:isReady()

    return ("ClientBuilder (%s)"):format(
      ready and "ready" or reason
    )
  end,
}

function lib.builder()
  local settings = {}

  settings.connection = {
    host = nil,
    tls = false,
    timeout = 10,
    bufferSize = 1024, -- 2 lines
    throttling = {
      maxDelay = 0,
      maxThroughput = math.huge,
      delayFunction = nil,
    },
    splitter = splitter,
    maxLineLength = 400,
    __ready = false,
  }

  settings.auth = {
    username = nil,
    nickname = nil,
    realname = nil,
    __ready = false,
  }

  settings.account = {
    nickname = nil,
    password = nil,
    __ready = true,
  }

  settings.bot = {
    channels = {},
    tracking = {
      users = false,
      modes = false,
      userInfo = false,
      account = false,
    },
    __ready = true,
  }

  settings.execution = {
    threaded = true,
    reconnect = true,
    catchErrors = false,
    __ready = true,
  }

  local client = {
    settings = settings,
    subscribers = {},
    features = {},
    botUser = nil,
    channels = {},
    users = {},
    capabilities = {},
    requestedCapabilities = {},
    unprocessedCapabilityRequests = 0,
    whoxQueue = {},
  }

  local addr = tostring(client):match("table: (.+)$")
  client.bus = eventBus.EventBus(
    client,
    ("IRC event bus (client %s)"):format(addr)
  )

  return setmetatable(client, builderMeta)
end

return lib
