local buffer = require("buffer")
local comp = require("computer")
local event = require("event")
local inet = require("internet")
local thread = require("thread")

local events = require("irc.event")
local handlers = require("irc.client.handlers")

local protocol = require("irc.protocol")
local Source = protocol.Source
local parseLine = protocol.parseLine

local enum = require("irc.enum")
local commands = enum.commands
local getCommandName = enum.getCommandName

local state = require("irc.state")
local User = state.User
local Channel = state.Channel

local caseTranslation = {
  ascii = {},
  rfc1459 = {["["] = "{", ["]"] = "}", ["\\"] = "|", ["~"] = "^"},
  ["rfc1459-strict"] = {["["] = "{", ["]"] = "}", ["\\"] = "|"},
}

local function nonBlockingRead(buf, ...)
  local success, status, reason = pcall(buf.read, buf, ...)

  if not success and status:sub(-7) == "timeout" then
    return ""
  elseif not success then
    -- propagate the error
    error(status)
  else
    return status, reason
  end
end

local clientMeta = {
  __index = {
    isChannel = function(self, target)
      if self.features.channelTypes then
        return not not self.features.channelTypes[target:sub(1, 1)]
      end

      return target:sub(1, 1) == "#"
    end,

    lowerCase = function(self, identifier)
      local caseMapping = self.features.caseMapping
      local translationTable = caseTranslation[caseMapping] or
                               caseTranslation.rfc1459

      return (identifier:lower():gsub("[][\\~]", translationTable))
    end,

    areEqual = function(self, id1, id2)
      return self:lowerCase(id1) == self:lowerCase(id2)
    end,

    getChannel = function(self, name, create)
      local lowercased = self:lowerCase(name)

      assert(self:isChannel(lowercased),
             ("not a channel name: %s"):format(name))

      if create and not self.channels[lowercased] then
        self.channels[lowercased] = Channel(self, name)
      end

      return self.channels[lowercased]
    end,

    getUser = function(self, source, create)
      if getmetatable(source) ~= "source" then
        source = Source(source)
      end

      local lowercased = self:lowerCase(source.nickname)

      assert(not self:isChannel(lowercased),
             ("source is a channel: %s"):format(source.nickname))

      if create and not self.users[lowercased] then
        self.users[lowercased] = User(self, source)
      end

      return self.users[lowercased]
    end,

    highestPrefix = function(self, ...)
      local args = {...}

      for _, value in ipairs(args) do
        args[value] = true
      end

      for _,  prefix in ipairs(self.features.membershipPrefixes and
                               self.features.membershipPrefixes.prefixes or
                               {"@", "+"}) do
        if args[prefix] then
          return prefix
        end
      end

      return true
    end,

    subscribe = function(self, ...)
      local handler = self.bus:subscribe(...)
      handler.userDefined = true

      return self, handler
    end,

    isConnected = function(self)
      if not self.socket then
        return nil, "not connected"
      end

      local status, reason = self.socket.finishConnect()

      if status == nil then
        self.socket = nil
        self.buffer:close()
      end

      return status, reason
    end,

    connect = function(self)
      if self:isConnected() ~= nil then
        return true, "already running"
      end

      local socketStream = inet.socket(self.settings.connection.host)
      self.socket = socketStream.socket

      self.__socketLock = true

      local timeout = comp.uptime() + self.settings.connection.timeout

      while true do
        local status, reason = self:isConnected()

        if status == true then
          break
        elseif status == nil then
          error(("connection failed: %s"):format(reason))
        end

        if comp.uptime() >= timeout then
          error("connection timed out")
        end

        os.sleep(0)
      end

      if self.settings.connection.tls then
        self.socket = require("tls").wrap(self.socket)
        socketStream.socket = self.socket
      end

      self.buffer = buffer.new("rwb", socketStream)
      self.buffer:setTimeout(0)
      self.buffer:setvbuf("line", self.settings.connection.bufferSize)

      self:__setupInternalHandlers()
      self.__socketLock = nil
      self.bus:push(events.client.connected(self))

      return true
    end,

    __processIncoming = function(self)
      while true do
        local line = nonBlockingRead(self.buffer)

        if not line then
          self.buffer:close()
          self.socket = nil
          self.bus:push(events.client.disconnected(self))

          return false
        end

        if line == "" then
          return true
        end

        local tags, source, command, parameters = parseLine(line)

        local eventData = {
          tags = tags,
          source = source,
          command = command,
          rawLine = line,
          table.unpack(parameters)
        }

        if not commands[command] then
          self.bus:push(events.client.unknownCommand(self, eventData))
        else
          local name = getCommandName(commands[command])
          eventData.command = commands[command]

          self.bus:push(events.irc.command(self, eventData))
          self.bus:push(events.irc.commands[name](self, eventData))
        end
      end
    end,

    __setupInternalHandlers = function(self)
      for _, params in ipairs(handlers) do
        local args = {table.unpack(params)}

        if params.coroutine then
          args[#args] = coroutine.create(args[#args])
        end

        local handler = self.bus:subscribe(table.unpack(args))

        if params.userDefined then
          handler.userDefined = true
        end

        table.insert(self.subscribers, handler)
      end
    end,

    run = function(self)
      assert(not self.running, "already running")
      self.running = true

      self.__internetReadyListener = function(evt, addr, id)
        if not self.running or not self.socket then
          return
        end

        if id == self.socket.id() then
          self:__processIncoming()
        end
      end

      if self.settings.execution.threaded then
        self.thread = thread.create(function()
          assert(self:connect())

          while self.running do
            local evt, addr, id = event.pull(0.05, "internet_ready")

            if evt then
              self.__internetReadyListener(evt, addr, id)
            elseif self.socket then
              -- reset notifier
              self.socket.finishConnect()
            end
          end
        end)

        return self.thread
      else
        event.listen("internet_ready", self.__internetReadyListener)
        assert(self:connect())

        return true
      end
    end,

    stop = function(self, reason)
      if not self.running then
        return false, "not running"
      end

      self.running = false

      if self:isConnected() then
        self.send:quit(reason)

        self.__socketLock = true

        -- Here we must be extra careful about avoiding deadlocks
        while #self.__writeScheduler.scheduler > 0 and
            self.thread:status() ~= "dead" do
          os.sleep(0.05)
        end

        if self.buffer then
          self.buffer:close()
          self.bus:push(events.client.disconnected(self))
        elseif self.socket then
          self.socket:close()
        end
      end

      -- update the status
      self:isConnected()
      self.__socketLock = nil

      self.bus:push(events.client.stopping(self))

      if self.settings.execution.threaded then
        if require("thread").current() ~= self.thread then
          self.thread:join()
        end
      else
        event.ignore("internet_ready", self.__internetReadyListener)
      end
    end,

    msg = function(self, target, message)
      return self.send:privmsg(target, message)
    end,

    notice = function(self, target, message)
      return self.send:notice(target, message)
    end,

    requestCapability = function(self, capability)
      if not self.capabilities[capability] and
          not self.requestedCapabilities[capability] then
        self.send:cap("REQ", capability)
        self.requestedCapabilities[capability] = true
        self.unprocessedCapabilityRequests =
          self.unprocessedCapabilityRequests + 1
      end
    end,

    whox = function(self, mask, format)
      assert(self.features.extendedWho, "server doesn't support WHOX")
      self.send:who(mask, format)

      local fields = {}

      for field in (format:match("%%(.*)$") or ""):gmatch(".") do
        fields[field] = true
      end

      table.insert(self.whoxQueue, fields)
    end,
  },

  __metatable = "client metatable",

  __tostring = function(self)
    return ("Client { socketId = %q, bus = %s }"):format(
      self.socket and self.socket.id(),
      self.bus
    )
  end,
}

return clientMeta
