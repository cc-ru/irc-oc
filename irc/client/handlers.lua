-- Lua 5.2 fallback
local utf8 = utf8 or {
  charpattern = "[\0-\x7F\xC2-\xF4][\x80-\xBF]*",
}

local events = require("irc.event")
local priority = events.priority

local isupportHandlers = require("irc.protocol.isupport")
local capHandlers = require("irc.protocol.capabilities")

local Prefix = require("irc.state").Prefix

local ReconnectCallable do
  local meta = {
    __call = function(self)
      if not self.client:isConnected() and self.client.running then
        return self.client:connect()
      end
    end,
  }

  function ReconnectCallable(client)
    local self = {
      client = client,
    }

    return setmetatable(self, meta)
  end
end

return {
  {events.client.error, priority.bottom, function(self, client, evt)
     error(evt.traceback, 0)
   end},

  {events.irc.commands.error, priority.bottom, function(self, client, evt)
     error(("Server returned an error: %s"):format(evt[1]))
   end, userDefined = true},

  {events.client.connected, priority.top,
   function(self, client, evt)
     client.botUser = client:getUser(client.settings.auth.nickname, true)

     client.send:cap("LS", "302")
     client.send:user(client.settings.auth.username, "0", "*",
                      client.settings.auth.realname)

     while true do
       client.send:nick(client.botUser.nickname)

       evt = self:await(
         {events.irc.commands.errNicknameinuse, priority.bottom},
         {events.irc.commands.errErroneusnickname, priority.bottom},
         {events.irc.commands.rplIsupport, priority.bottom}
       )

       if evt.__class == events.irc.commands.errNicknameinuse then
         local suffix = client.botUser.nickname:sub(
           #client.settings.auth.nickname + 1
         )

         if suffix == "" then
           suffix = "0"
         end

         client.botUser.nickname = client.botUser.nickname ..
           tostring(tonumber(suffix) + 1)
       elseif evt.__class == events.irc.commands.errErroneusnickname then
         error("erroneous nickname")
       else
         client.bus:push(events.client.registered(client))
         return
       end
     end
   end, coroutine = true},

  {{events.irc.commands.cap, priority.bottom},
   {events.client.registered, priority.top},
   function(self, client, evt)
     while evt[2] ~= "LS" or evt[3] == "*" do
       if evt.__class == events.client.registered then
         return
       end

       evt = self:await()
     end

     repeat
       evt = self:await()

       if evt.__class == events.client.registered then
         return
       end
     until client.unprocessedCapabilityRequests > 0

     client.send:cap("END")
     client.bus:push(events.capability.negotiated(client))
   end, coroutine = true},

  {events.client.disconnected, priority.bottom, function(self, client, evt)
     for _, subscriber in ipairs(client.subscribers) do
       subscriber:remove()
     end

     for k, v in pairs(client.channels) do
       v.joined = false
     end

     client.subscribers = {}
     client.features = {}
     client.botUser = nil
     client.channels = {}
     client.users = {}
     client.capabilities = {}
     client.requestedCapabilities = {}
     client.unprocessedCapabilityRequests = 0
     client.whoxQueue = {}
     client.__writeScheduler.scheduler:clear()

     if client.running and client.settings.execution.reconnect then
       client.__reconnectScheduler:push(ReconnectCallable(client))
     else
       client:stop()
     end
   end},

  {events.irc.commands.ping, priority.top, function(self, client, evt)
     client.send:pong(evt[1])
   end},

  {events.irc.commands.rplIsupport, priority.top, function(self, client, evt)
     for i = 2, math.huge do
       if not evt[i + 1] then
         break
       end

       local key, value = evt[i]:match("^([^=]+)=(.*)$")

       if not key then
         key = evt[i]
       end

       if isupportHandlers[key] then
         isupportHandlers[key](client, value)
       end
     end
   end},

  {events.irc.commands.cap, priority.high, function(self, client, evt)
     if evt[2] == "LS" or evt[2] == "NEW" then
       local capString = evt[3]

       if evt[3] == "*" then
         capString = evt[4]
       end

       for cap in capString:gmatch("%S+") do
         local name, value = cap:match("^([^=]+)=(.*)$")

         if not name then
           name = cap
           value = true
         end

         if not client.capabilities[name] then
           client.bus:push(events.capability.new(client, {
             capability = name,
             value = value
           }))
         end
       end
     elseif evt[2] == "ACK" then
       for capability in evt[3]:gmatch("%S+") do
         local removed = nil

         if capability:sub(1, 1) == "-" then
           capability = capability:sub(2)
           removed = true
         end

         if removed and client.capabilities[capability] then
           client.capabilities[capability] = nil
           client.bus:push(events.capability.removed(client, {
             capability = capability
           }))
         elseif not removed then
           client.capabilities[capability] = true
           client.bus:push(events.capability.acknowledged(client, {
             capability = capability,
           }))
         end
       end
     elseif evt[2] == "NAK" then
       for capability in evt[3]:gmatch("%S+") do
         client.bus:push(events.capability.notAcknowledged(client, {
           capability = capability
         }))
       end
     elseif evt[2] == "DEL" then
       for capability in evt[3]:gmatch("%S+") do
         client.capabilities[capability] = nil
         client.bus:push(events.capability.removed(client, {
           capability = capability,
         }))
       end
     end
   end},

  {events.client.registered, priority.bottom, function(self, client, evt)
     if client.settings.account.nickname then
       client:msg("NickServ", ("IDENTIFY %s %s"):format(
         client.settings.account.nickname,
         client.settings.account.password
       ))
     end

     client.bus:push(events.client.authenticated(client))
   end},

  {events.client.authenticated, priority.top, function(self, client, evt)
     for channel, key in pairs(client.settings.bot.channels) do
       if key == true then
         client.send:join(channel)
       else
         client.send:join(channel, key)
       end
     end
   end},

  {events.irc.commands.privmsg, priority.bottom, function(self, client, evt)
     local eventData = {
       originalEvent = evt,
       tags = evt.tags,
       source = evt.source,
       target = evt[1],
       message = evt[2],
     }

     if evt[2]:sub(1, 1) == "\1" then
       -- CTCP request
       local command, params = evt[2]:match("^\1([^\1%s]+)\1?%s?([^\1]*)\1?$")

       if command then
         eventData.ctcpCommand = command:upper()
         eventData.ctcpParams = params
         client.bus:push(events.irc.ctcpRequest(client, eventData))
         return
       end
     end

     client.bus:push(events.irc.message(client, eventData))
   end},

  {events.irc.commands.notice, priority.bottom, function(self, client, evt)
     local eventData = {
       originalEvent = evt,
       tags = evt.tags,
       source = evt.source,
       target = evt[1],
       message = evt[2]
     }

     if evt[2]:sub(1, 1) == "\1" then
       -- CTCP response
       local command, params = evt[2]:match("^\1([^\1%s]+)\1?%s?([^\1]*)\1?$")

       if command then
         eventData.ctcpCommand = command:upper()
         eventData.ctcpParams = params
         client.bus:push(events.irc.ctcpResponse(client, eventData))
         return
       end
     end

     client.bus:push(events.irc.notice(client, eventData))
   end},

  {events.irc.commands.rplNotopic, priority.high, function(self, client, evt)
     local channel = client:getChannel(evt[2], true)
     channel.topic = nil
   end},

  {events.irc.commands.rplTopic, priority.high, function(self, client, evt)
     local channel = client:getChannel(evt[2], true)
     channel.topic = evt[3]
   end},

  {events.irc.commands.join, priority.high, function(self, client, evt)
     local channel = client:getChannel(evt[1], true)
     local user = client:getUser(evt.source, true)

     if user == client.botUser then
       channel.joined = true

       if client.settings.bot.tracking.modes then
         -- request channel modestring
         client.send:mode(channel.name)
       end

       if client.features.extendedWho then
         if client.settings.bot.tracking.userInfo then
           client:whox(channel.name, "%tcuihsnfdlarm")
         elseif client.settings.bot.tracking.account then
           client:whox(channel.name, "%cna")
         end
       elseif client.settings.bot.tracking.userInfo then
         client.send:who(channel.name)
       end
     else
       if client.settings.bot.tracking.users then
         channel.users[user] = Prefix()
         user.channels[channel] = channel.users[user]
       end

       if client.settings.bot.tracking.userInfo or
           client.settings.bot.tracking.account then
         if client.capabilities["extended-join"] then
           if client.settings.bot.tracking.userInfo then
             user.realname = evt[3]
           end

           if client.settings.bot.tracking.account then
             user.account = evt[2] ~= "*" and evt[2]
           end
         elseif client.features.extendedWho then
           if client.settings.bot.tracking.userInfo then
             client:whox(user.nickname, "%tcuihsnfdlarm")
           else
             client:whox(user.nickname, "%cna")
           end
         end
       end
     end
   end},

  {events.irc.commands.part, priority.high, function(self, client, evt)
    local channel = client:getChannel(evt[1], true)
    local user = client:getUser(evt.source, true)

     if user == client.botUser then
       channel.joined = false
     end

     if client.settings.bot.tracking.users then
       channel.users[user] = nil
       user.channels[channel] = nil
     end
   end},

  {events.irc.commands.quit, priority.high, function(self, client, evt)
     local user = client:getUser(evt.source)

     if user and client.settings.bot.tracking.users then
       for channel in pairs(user.channels) do
         channel.users[user] = nil
         user.channels[channel] = nil
       end
     end
   end},

  {events.irc.commands.rplChannelmodeis, priority.high,
   function(self, client, evt)
     if not client.settings.bot.tracking.modes then
       return
     end

     local channel = client:getChannel(evt[2], true)
     local modeString = evt[3]

     channel.modes:reset()
     channel.modes:change(modeString, table.unpack(evt:get(), 4))
   end},

  {events.irc.commands.rplUmodeis, priority.high, function(self, client, evt)
     if not client.settings.bot.tracking.modes then
       return
     end

     client.botUser.modes:reset()
     client.botUser.modes:change(evt[2])
   end},

  {events.irc.commands.mode, priority.high, function(self, client, evt)
     local membershipPrefixes = client.features.membershipPrefixes

     if not client.settings.bot.tracking.modes then
       return
     end

     if not client:isChannel(evt[1]) then
       if client:getUser(evt[1]) == client.botUser then
         client.botUser.modes:change(evt[2])
       end

       return
     end

     local channel = client:getChannel(evt[1])
     local modeString = evt[2]

     local actions = channel.modes:parse(modeString, table.unpack(evt:get(), 3))
     channel.modes:change(actions)

     local updateUsers = client.settings.bot.tracking.users

     if updateUsers and membershipPrefixes then
       updateUsers = {}

       for _, mode in ipairs(membershipPrefixes.modes) do
         if actions[mode] then
           for arg, set in pairs(actions[mode]) do
             local user = client:getUser(arg)

             if user and channel.users[user] then
               if client.capabilities["multi-prefix"] then
                 channel.users[user][mode] = set or nil
               elseif set then
                 channel.users[user][mode] = true
               else
                 table.insert(updateUsers, arg)
               end
             end
           end
         end
       end
     end

     if updateUsers and (updateUsers == true or #updateUsers > 0) and
         not client.capabilities["multi-prefix"] then
       client.send:names(channel.name)
     end
   end},

  {events.irc.commands.rplNamreply, priority.high, function(self, client, evt)
     local tracking = client.settings.bot.tracking

     while true do
       local channel = client:getChannel(evt[3], true)

       if evt[2] == "=" then
         channel.type = "public"
       elseif evt[2] == "@" then
         channel.type = "secret"
       elseif evt[2] == "*" then
         channel.type = "private"
       end

       if tracking.users then
         local members = {}

         repeat
           for name in evt[4]:gmatch("%S+") do
             local prefixes = Prefix(client)
             local membershipPrefixes = client.features.membershipPrefixes
             local i = 1

             while true do
               local prefix = name:sub(i, i)

               if not membershipPrefixes or
                   not membershipPrefixes.prefixes[prefix] then
                 break
               end

               prefixes[membershipPrefixes.prefixes[prefix]] = true
               i = i + 1
             end

             local nickname = name:sub(i)

             local member = client:getUser(nickname, true)
             members[member] = tracking.modes and prefixes or true
           end

           evt = self:await({events.irc.commands.rplNamreply, priority.high},
                            {events.irc.commands.rplEndofnames,
                             priority.top})
         until evt.__class == events.irc.commands.rplEndofnames

         for _, user in pairs(client.users) do
           user.channels[channel] = members[user]
         end

         for user in pairs(channel.users) do
           channel.users[user] = members[user]
         end

         -- add new members
         for member, prefix in pairs(members) do
           channel.users[member] = prefix
         end
       end

       evt = self:await()
     end
   end, coroutine = true},

  {events.irc.command, priority.high, function(self, client, evt)
     if client.settings.bot.tracking.users and evt.source and
         evt.source.username then
       local user = client:getUser(evt.source, true)
       user.username = evt.source.username
       user.hostname = evt.source.hostname
     end
   end},

  {events.irc.commands.nick, priority.high, function(self, client, evt)
     local user = client:getUser(evt.source)

     if user then
       client.users[client:lowerCase(user.nickname)] = nil
       user.nickname = evt[1]
       client.users[client:lowerCase(user.nickname)] = user
     end
   end},

  {events.capability.new, priority.high, function(self, client, evt)
     local handler = capHandlers[evt.capability]

     if handler and (type(handler) ~= "table" or
                     not handler.new or
                     handler.new(client, evt.capability, evt.value)) then
       client:requestCapability(evt.capability)
     end
   end},

  {events.capability.acknowledged, priority.high, function(self, client, evt)
     if client.requestedCapabilities[evt.capability] then
       client.requestedCapabilities[evt.capability] = nil
       client.unprocessedCapabilityRequests =
         client.unprocessedCapabilityRequests - 1
     end

     local handler = capHandlers[evt.capability]

     if type(handler) == "table" and handler.add then
       handler.add(client, evt.value)
     end
   end},

  {events.capability.notAcknowledged, priority.high, function(self, client, evt)
     if client.requestedCapabilities[evt.capability] then
       client.requestedCapabilities[evt.capability] = nil
       client.unprocessedCapabilityRequests =
         client.unprocessedCapabilityRequests - 1
     end
   end},

  {events.capability.removed, priority.high, function(self, client, evt)
     local handler = capHandlers[evt.capability]

     if type(handler) == "table" and handler.remove then
       handler.remove(client)
     end
   end},

  {events.irc.commands.rplWhoreply, priority.high, function(self, client, evt)
     if not client.settings.bot.tracking.userInfo then
       return
     end

     local channel, username, hostname, server, nickname, flags, lastParam =
       table.unpack(evt:get())

     local user = client:getUser(nickname)

     if not user then
       return
     end

     local awayStatus, oper, prefixes = flags:match("^([HG]?)(%*?)(.*)$")
     local hopCount, realname = lastParam:match("^(%d+) (.*)$")

     if awayStatus == "H" then
       awayStatus = false
     elseif awayStatus == "G" then
       awayStatus = true
     else
       awayStatus = nil
     end

     oper = oper == "*" or nil
     hopCount = tonumber(hopCount)

     user.username = username
     user.hostname = hostname
     user.realname = realname
     user.server = server
     user.away = awayStatus
     user.oper = oper
     user.hopCount = hopCount

     channel = client:getChannel(channel)

     if channel then
       channel.users[user]:parse(prefixes)
     end
   end},

  {events.irc.commands.rplWhospcrpl, priority.top, function(self, client, evt)
     while true do
       local fields = table.remove(client.whoxQueue, 1)

       while evt.__class ~= events.irc.commands.rplEndofwho do
         local i = 2

         if fields.t then
           evt.queryType = evt[i]
           i = i + 1
         end

         if fields.c then
           evt.channelName = evt[i]
           i = i + 1
         end

         if fields.u then
           evt.username = evt[i]
           i = i + 1
         end

         if fields.i then
           evt.ip = evt[i]
           i = i + 1
         end

         if fields.h then
           evt.hostname = evt[i]
           i = i + 1
         end

         if fields.s then
           evt.server = evt[i]
           i = i + 1
         end

         if fields.n then
           evt.nickname = evt[i]
           i = i + 1
         end

         if fields.f then
           local awayStatus, oper, prefix = evt[i]:match("^([HG]?)(%*?)(.*)$")

           if awayStatus == "G" then
             awayStatus = true
           elseif awayStatus == "H" then
             awayStatus = false
           else
             awayStatus = nil
           end

           oper = oper == "*" or nil
           prefix = Prefix(client):parse(prefix)

           evt.away = awayStatus
           evt.oper = oper
           evt.prefix = prefix

           i = i + 1
         end

         if fields.d then
           evt.hopCount = tonumber(evt[i])
           i = i + 1
         end

         if fields.l then
           evt.idle = tonumber(evt[i])
           i = i + 1
         end

         if fields.a then
           evt.account = evt[i] ~= "0" and evt[i]
           i = i + 1
         end

         if fields.o then
           evt.operRank = tonumber(evt[i])
           i = i + 1
         end

         if fields.r then
           evt.realname = evt[i]
           i = i + 1
         end

         evt = self:await()
       end
     end
   end, coroutine = true},

  {events.irc.commands.rplWhospcrpl, priority.high, function(self, client, evt)
     if not evt.nickname then
       return
     end

     local user = client:getUser(evt.nickname)

     if not user then
       return
     end

     if client.settings.bot.tracking.account then
       if evt.account ~= nil then
         user.account = evt.account
       end
     end

     if not client.settings.bot.tracking.userInfo then
       return
     end

     user.username = evt.username or user.username
     user.ip = evt.ip or user.ip
     user.hostname = evt.hostname or user.hostname
     user.server = evt.server or user.server
     user.away = evt.away or user.away
     user.oper = evt.oper or user.oper
     user.hopCount = evt.hopCount or user.hopCount
     user.idle = evt.idle or user.idle
     user.operRank = evt.operRank or user.operRank
     user.realname = evt.realname or user.realname

     if evt.channelName and evt.prefix then
       local channel = client:getChannel(evt.channelName)

       if channel and channel.users[user] then
         channel.users[user]:set(evt.prefix)
       end
     end
   end},

  {events.irc.commands.account, priority.high, function(self, client, evt)
     if not client.settings.bot.tracking.account then
       return
     end

     local user = client:getUser(evt.source)

     if not user then
       return
     end

     user.account = evt[1] ~= "*" and evt[1]
   end},
}
