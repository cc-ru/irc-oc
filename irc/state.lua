local Source = require("irc.protocol").Source

local Modes do
  local function setAction(tbl, key, value, set)
    if tbl[key] and not set then
      tbl[key] = nil
    else
      tbl[key] = value
    end
  end

  local meta = {
    __tostring = function(self)
      local args = {}
      local modes = {}

      for mode, value in pairs(self) do
        if mode ~= "client" and mode ~= "channel" then
          if type(value) == "table" then
            for k in pairs(value) do
              table.insert(modes, mode)
              table.insert(args, k)
            end
          else
            table.insert(modes, mode)

            if value ~= true then
              table.insert(args, value)
            end
          end
        end
      end

      table.insert(args, 1, table.concat(modes))

      return ("Modes { %s%s }"):format(
        #args[1] > 0 and "+" or "none",
        table.concat(args, " ")
      )
    end,

    __index = {
      getGroup = function(self, mode)
        if self.channel then
          local group = "never"

          if self.client.features.channelModes then
            group = self.client.features.channelModes.modes[mode] or group
          end

          if self.client.features.membershipPrefixes and
              self.client.features.membershipPrefixes.modes[mode] then
            group = "membership"
          end

          return group
        end

        return "never"
      end,

      __set = function(self, mode, arg)
        local group = self:getGroup(mode)

        if group == "server" and arg then
          self[mode] = self[mode] or {}
          self[mode][arg] = true
        elseif group == "always" or group == "onlySet" then
          self[mode] = arg
        elseif group == "never" then
          self[mode] = true
        end
      end,

      __unset = function(self, mode, arg)
        local group = self:getGroup(mode)

        if group == "server" and arg then
          self[mode] = self[mode] or {}
          self[mode][arg] = nil
        elseif group == "always" or group == "onlySet" or group == "never" then
          self[mode] = nil
        end
      end,

      parse = function(self, modeString, ...)
        local result = {}
        local i = 1

        for action, modes in modeString:gmatch("([+-])([^+-]*)") do
          local set = action == "+"

          for mode in modes:gmatch(".") do
            local group = self:getGroup(mode)
            local arg = nil

            if group == "server" or group == "always" or
                group == "membership" or
                set and group == "onlySet" then
              arg = select(i, ...)
              i = i + 1
            end

            if arg and (group == "server" or group == "membership") then
              result[mode] = result[mode] or {}

              setAction(result[mode], arg, set, set)
            elseif group == "always" then
              setAction(result, mode, set, set)
            elseif group == "onlySet" then
              setAction(result, mode, set and arg, set)
            elseif group == "never" then
              setAction(result, mode, set, set)
            end
          end
        end

        return result
      end,

      change = function(self, actions, ...)
        if type(actions) == "string" then
          actions = self:parse(actions, ...)
        end

        for mode, v in pairs(actions) do
          if type(v) == "table" then
            for value, set in pairs(v) do
              if set then
                self:__set(mode, value)
              else
                self:__unset(mode, value)
              end
            end
          else
            if v then
              self:__set(mode, v)
            else
              self:__unset(mode, v)
            end
          end
        end
      end,

      reset = function(self)
        for k, v in pairs(self) do
          if k ~= "client" and k ~= "channel" then
            self[k] = nil
          end
        end
      end,
    },
  }

  function Modes(client, isChannel)
    local self = {
      client = client,
      channel = isChannel,
    }

    return setmetatable(self, meta)
  end
end

local Prefix do
  local meta = {
    __tostring = function(self)
      local membershipPrefixes = self.__client.features.membershipPrefixes

      if membershipPrefixes then
        membershipPrefixes = membershipPrefixes.prefixes
      else
        membershipPrefixes = {["@"] = "o", ["+"] = "v"}
      end

      for prefix, mode in pairs(membershipPrefixes) do
        if self[mode] then
          return prefix
        end
      end

      return ""
    end,

    __eq = function(self, other)
      return tostring(self) == tostring(other)
    end,

    __lt = function(self, other)
      local prefix = tostring(self)

      return self.__client:highestPrefix(prefix, tostring(other)) ~= prefix
    end,

    __le = function(self, other)
      local prefix = tostring(self)

      return self == tostring(other) or self < other
    end,

    __index = {
      parse = function(self, prefixString)
        local membershipPrefixes = self.__client.features.membershipPrefixes

        if not membershipPrefixes then
          return self
        end

        self:reset()

        for prefix in prefixString:gmatch(".") do
          local mode = membershipPrefixes.prefixes[prefix]

          if mode then
            self[mode] = true
          end
        end

        return self
      end,

      set = function(self, other)
        self:reset()

        for mode in pairs(other) do
          if mode ~= "__client" then
            self[mode] = true
          end
        end
      end,

      reset = function(self)
        for k in pairs(self) do
          if k ~= "__client" then
            self[k] = nil
          end
        end
      end,
    },
  }

  function Prefix(client)
    return setmetatable({
      __client = client,
    }, meta)
  end
end

local User do
  local meta = {
    __tostring = function(self)
      return ("User { nickname = %q }"):format(self.nickname)
    end,

    __eq = function(self, other)
      if type(other) == "string" then
        return self.client:areEqual(self.nickname, Source(other).nickname)
      end

      if getmetatable(self) == getmetatable(other) then
        return self.client == other.client and
               self.client:areEqual(self.nickname, other.nickname)
      end

      if getmetatable(other) == "source" then
        return self.client:areEqual(self.nickname, other.nickname)
      end

      return false
    end,
  }

  function User(client, source)
    local self = {
      client = client,
      nickname = source.nickname,
      username = source.username,
      hostname = source.hostname,
      realname = nil,
      server = nil,
      away = nil,
      oper = nil,
      hopCount = nil,
      ip = nil,
      account = nil,
      operRank = nil,
      channels = client.settings.bot.tracking.users and {} or nil,
      modes = client.settings.bot.tracking.modes and Modes(client, false) or nil
    }

    return setmetatable(self, meta)
  end
end

local Channel do
  local meta = {
    __tostring = function(self)
      return ("Channel { name = %q }"):format(self.name)
    end,

    __eq = function(self, other)
      if type(other) == "string" then
        return self.client:areEqual(self.nickname, other)
      end

      if getmetatable(self) == getmetatable(other) then
        return self.client == other.client and
               self == other.nickname
      end

      return false
    end,
  }

  function Channel(client, name)
    local self = {
      client = client,
      name = name,
      users = client.settings.bot.tracking.users and {} or nil,
      topic = nil,
      joined = false,
      type = nil,
      modes = client.settings.bot.tracking.modes and Modes(client, true) or nil
    }

    return setmetatable(self, meta)
  end
end

return {
  User = User,
  Channel = Channel,
  Modes = Modes,
  Prefix = Prefix,
}
